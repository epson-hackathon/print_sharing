## We Make Print Sharing Service
our product topology is shown in **topology.JPG**

### Rule

- when you **add new feature**, 

1. create **new issue** which describes the feature you will develop
2. create **corresponding branch** with that issue
3. work on that branch
4. done
5. **create merge request** of that branch to master branch
6. put reviewer to someone 


- when you **commit**,
put message as 

- [ADD]
- [FIX]
- [REF]
- [UPD]

etc

and then add short description of your work.

#### Example
git commit -m "[ADD] my-awsome feature"
