import requests
import json
import os

CLIENT_ID = "0c58ebbe227e496aa1c825732616b092"
SECRET_PASS = "UdBGE6P6eR7FEfc5ZN6JtFrOLVlgEyRw5HmTNx1guPdVTOvF491akIAWIvb9AOrv"
PRINTER_NUM = '10'
PRINTER_EMAIL = "epsonhacktrek{}@print.epsonconnect.com".format(PRINTER_NUM)

FILE_PATH='./1.pdf'
FILE_NAME= "1.pdf"


def authentication(client_id=CLIENT_ID, secret_pass=SECRET_PASS, printer_email=PRINTER_EMAIL):
    """Get authentication of the printer
    Args:
        client_id: client's id
        secrest_pass: client's secret password
        printer_email: printer's email

    Returns:
        access_token: access token of the selected printer
        printer_id: id of the selected printer
    """

    base_url = "https://api.epsonconnect.com/api/1/printing/oauth2/auth/token?subject=printer"
    headers = {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
    params = {
    'grant_type': 'password',
    'username': printer_email,
    'password': ''
    }
    res = requests.post(base_url, data=params, headers=headers, auth=(CLIENT_ID, SECRET_PASS))
    print(res.status_code)

    json_auth = json.loads(res.text)
    access_token = json_auth['access_token']
    printer_id = json_auth['subject_id']

    return access_token, printer_id

def create_job(access_token, printer_id):
    """Create a print job
    Args:
        access_token: access token of the selected printer
        printer_id: id of the selected printer

    Returns:
        upload_uri: uri of the job
        job_id: id of the job
    """

    base_url = "https://api.epsonconnect.com/api/1/printing/printers/"+printer_id+"/jobs"
    headers = {'Authorization': 'Bearer '+access_token, 'Content-Type': 'application/json; charset=utf8'}
    obj = {
     "job_name" : "password",
     "print_mode" : "document"
    }
    json_data = json.dumps(obj).encode("utf-8")
    res = requests.post(base_url, data=json_data, headers=headers)
    print(res.status_code)

    json_job = json.loads(res.text)
    upload_uri = json_job['upload_uri']
    job_id = json_job['id']

    return upload_uri, job_id


def upload_image(upload_uri, file_path=FILE_PATH, file_name=FILE_NAME):
    """Upload file to be printed out
    Args:
        upload_uri: job's uri
        file_path: path of the file to be printed out
        file_name: name of the file to be printed out
    """

    content_type = 'application/octet-stream'
    uri = upload_uri
    base_url = uri + "&File=" + file_name
    file_size = os.path.getsize(file_path)
    headers = {'Content-Length': str(file_size), 'Content-Type': content_type}
    file = {'upload_file': open(file_path, 'rb')}
    res = requests.post(base_url, files=file, headers=headers)
    print(res.status_code)

def print_exec(printer_id, job_id, access_token):
    """Execute print the uploaded file
    Args:
        printer_id: id of the selected printer
        job_id: id of the job
        access_token: access token of the selected printer
    """

    uri = "https://api.epsonconnect.com/api/1/printing/printers/"+printer_id+"/jobs/"+job_id+"/print"
    headers = {'Authorization': 'Bearer '+access_token, 'Content-Type': 'application/json; charset=utf8'}
    res = requests.post(uri, headers=headers)
    print(res.status_code)

def run_printer(file_path=FILE_PATH, file_name=FILE_NAME):
    """Run the procedure of printing file.
    Args:
        file_path: path of the file to be printed out
        file_name: name of the file to be printed out
    """

    access_token, printer_id = authentication()
    upload_uri, job_id = create_job(access_token, printer_id)
    upload_image(upload_uri,file_path, file_name)
    print_exec(printer_id, job_id, access_token)

if __name__ == '__main__':
    run_printer()
